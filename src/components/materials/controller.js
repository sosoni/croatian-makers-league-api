const promise = require('bluebird');

const materialsService = require('./service');
const exception = require('../../common/exceptions/exceptions');
const logger = require('../../logger');

class MaterialsController {

    constructor() { }

    uploadMaterial(req, res) {
        let errorDetails = [];

        materialsService.saveFile(req, 'materials', (output) => {
            if (output instanceof exception) {
                logger.error(output);
                res.status(output.httpCode).send(output);
                return false;
            }

            promise.each(output, (data) => {
                return materialsService.createMaterial(data).catch((err) => {
                    errorDetails.push(err.message);
                });
            }).finally(() => {
                if (errorDetails.length) {
                    res.status(400).send(errorDetails);
                } else {
                    res.sendStatus(204);
                }
            });
        });
    }

    deleteMaterial(req, res) {
        materialsService.getMaterial(req.params.materialId).then((response) => {
            let materialName = response[0].name;

            return materialsService.deleteMaterial(req.params.materialId).then(() => {
                return materialsService.deleteFile(materialName, 'materials').then(() => {
                    res.sendStatus(200);
                });
            });
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    downloadMaterial(req, res) {
        materialsService.getMaterial(req.params.materialId).then((response) => {
            let materialName = response[0].name;
            let file = cml.path + '/upload/materials/' + materialName;
            res.set('Content-Disposition', materialName);
            res.download(file, materialName);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    updateMaterial(req, res) {
        let materialVisibility = req.body.visibility;

        materialsService.updateMaterial(req.params.materialId, materialVisibility).then(() => {
            res.sendStatus(200);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    getMaterials(req, res) {
        let institutionId = req.query.institutionId;

        materialsService.getMaterials(institutionId).then((result) => {
            let filteredResults = [];
            if(result.length > 0 && institutionId) {
                filteredResults = this.filterInstitutionMaterials(result, institutionId);
                res.status(200).send(filteredResults);
            } else {
                res.status(200).send(result);
            }
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    filterInstitutionMaterials(materials, institutionId) {
        let filteredMaterials = [];

        materials.forEach((material) => {
            let materialVisiblity = material.visibility.split(',');
            if(materialVisiblity.indexOf(institutionId) !== -1) {
                filteredMaterials.push(material);
            }
        });

        return filteredMaterials;
    }
}

module.exports = new MaterialsController();