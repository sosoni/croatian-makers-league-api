const express = require('express');
const router = express.Router();
const materialsController = require('./controller');

router.get('/all', (req, res) => {
    materialsController.getMaterials(req, res);
});

router.delete('/:materialId', (req, res) => {
    materialsController.deleteMaterial(req, res);
});

router.post('/upload', (req, res) => {
    materialsController.uploadMaterial(req, res);
});

router.get('/download/:materialId', (req, res) => {
    materialsController.downloadMaterial(req, res);
});

router.put('/:materialId', (req, res) => {
    materialsController.updateMaterial(req, res);
});

module.exports = router;