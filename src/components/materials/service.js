const FileParser = require('busboy');
const path = require('path');
const promise = require('bluebird');
const fs = require('fs');
const _ = require('lodash');

const settings = require('../../../settings');
const db = require('../../db');
const logger = require('../../logger');
const exception = require('../../common/exceptions/exceptions');
const exceptionCodes = require('../../common/exceptions/exception-codes');
const unlinkAsync = promise.promisify(require('fs').unlink);

class MaterialsService {

    constructor() { }

    createMaterial(data) {
        let visibility = data.visibility || 0;
        let query = "INSERT INTO materials SET name = ?, visibility = ?, extension = ?, encoding = ?, mime_type = ?, size = ?";
        let queryData = [data.name, visibility, data.extension, data.encoding, data.mimeType, data.size];

        return db.queryAsync(query, queryData).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    deleteMaterial(materialId) {
        let query = "DELETE FROM materials WHERE id = ?";
        let queryData = [materialId];

        return db.queryAsync(query, queryData).then((response) => {
            if (!response.affectedRows) {
                throw new exception(exceptionCodes.NONEXISTENT_ENTITY);
            } else {
                return response;
            }
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    updateMaterial(materialId, visibility) {
        let query = "UPDATE materials SET visibility = ? WHERE id = ?";
        let queryData = [visibility, materialId];

        return db.queryAsync(query, queryData).then((response) => {
            if (!response.affectedRows) {
                throw new exception(exceptionCodes.NONEXISTENT_ENTITY);
            }
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    getMaterials() {
        let query = "SELECT * FROM materials ORDER BY created_at DESC";

        return db.queryAsync(query).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    getMaterial(materialId) {
        let query = "SELECT * FROM materials WHERE id = ?";

        return db.queryAsync(query, [materialId]).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    deleteFile(name, locationPath) {
        let filePath = cml.path + '/upload/' + locationPath + '/' + name;

        return unlinkAsync(filePath).then().catch((err) => {
            logger.error(err);
            throw new exception(exceptionCodes.NONEXISTENT_FILE);
        });
    }

    saveFile(req, locationPath, onFinish) {
        let fileParser = new FileParser({
            headers: req.headers,
            limits: {
                files: settings.fileUpload.maxFiles,
                fileSize: settings.fileUpload.fileLimit * settings.fileUpload.maxFiles
            }
        });
        let output = [];
        let fileData = {};
        let fileVisibility = [];

        fileParser.on('file', (fieldName, stream, fileName, encoding, mimeType) => {
            let fileSize = 0;
            let filePath = cml.path + '/upload/' + locationPath + '/' + fileName;

            if (fs.existsSync(filePath)) {
                stream.resume();
                output = new exception(exceptionCodes.FILE_EXISTS);
            } else {
                stream.on('data', function (data) {
                    if (data.length) {
                        fileSize += data.length;
                    }
                });

                stream.on('end', function () {
                    fileData = {};
                    fileData.name = fileName;
                    fileData.extension = path.extname(fileName);
                    fileData.encoding = encoding;
                    fileData.mimeType = mimeType;
                    fileData.size = fileSize;
                    output.push(fileData);
                });

                stream.pipe(fs.createWriteStream(cml.path + '/upload/' + locationPath + '/' + fileName));
            }
        });

        fileParser.on('field', function (fieldName, value) {
            let propNameIndex = fieldName.indexOf('[');
            let fileName = fieldName.substring(propNameIndex+1, fieldName.length-1);
            fileVisibility.push({
                visibility : value,
                name : fileName
            });
        });

        fileParser.on('filesLimit', function () {
            output = new exception(exceptionCodes.MAX_FILES_UPLOADS_EXCEEDED);
        });

        fileParser.on('finish', () => {
            let parsedData = output.map((data) => {
                let filteredData = _.find(fileVisibility, { 'name': data.name });

                if(filteredData) {
                    _.extend(data, filteredData);
                }
                
                return data;
            });

            onFinish.call(this, parsedData);
        });

        return req.pipe(fileParser);
    }
}

module.exports = new MaterialsService();