const express = require('express');
const router = express.Router();
const passwordController = require('./controller');

router.post('/reset', (req, res) => {
    passwordController.resetPassword(req, res);
});

router.get('/reset_check', (req, res) => {
    passwordController.checkPasswordReset(req, res);
});

router.post('/set_new', (req, res) => {
    passwordController.setNewPassword(req, res);
});

module.exports = router;