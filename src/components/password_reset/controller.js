const moment = require('moment');

const emailer = require('../../common/emailer/emailer');
const passwordService = require('./service');
const settings = require('../../../settings');

class PasswordController {
    constructor() { }

    resetPassword(req, res) {
        let body = req.body;

        passwordService.resetPassword(body.email).then((uuid) => {
            // WARNING: Disable emailer in test phase (settings.js)
            // Inform user about password reset
            emailer.sendPasswordReset({ email: body.email, href: body.href + uuid });

            res.sendStatus(200);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    checkPasswordReset(req, res) {
        let uuid = req.query.uuid;

        passwordService.checkPaswordReset(uuid).then((response) => {
            let timestamp = moment(response.timestamp).unix();
            let expiryTreshold = settings.passwordReset.expiryDuration;
            let currentTimestamp = moment().unix();

            if (currentTimestamp - timestamp > expiryTreshold) {
                res.sendStatus(400);
            } else {
                res.status(200).send(response);
            }
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    setNewPassword(req, res) {
        let data = {
            email: req.body.data.email,
            password: req.body.password,
            uuid: req.body.data.req_uuid
        };

        passwordService.setNewPassword(data).then(() => {
            res.sendStatus(200);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }
}

module.exports = new PasswordController();