const db = require('../../db');
const logger = require('../../logger');
const exception = require('../../common/exceptions/exceptions');
const exceptionCodes = require('../../common/exceptions/exception-codes');
const uuidGenerator = require('uuid/v4');

class PasswordService {
    constructor() { }

    resetPassword(email) {
        let queryPasswordResetPrefix;
        let query = 'SELECT * FROM users WHERE email = ? AND authorized = 1 LIMIT 1';
        let queryData = [email];

        return db.queryAsync(query, queryData)
            // Check if user exist
            .then((response) => {
                if (response.length) return true;

                throw new exception(exceptionCodes.EMPTY_DB_RESULT);
            })
            // Check if user is in password reset table
            .then(() => {
                let query = 'SELECT * FROM password_reset WHERE email = ? LIMIT 1';
                let queryData = [email];

                return db.queryAsync(query, queryData).then((response) => {
                    queryPasswordResetPrefix = response.length ? 'UPDATE' : 'INSERT INTO';
                });
            })
            // Add to password reset table
            .then(() => {
                let uuid = uuidGenerator();
                let query = queryPasswordResetPrefix + ' password_reset SET req_uuid = ?, email = ?';
                let queryData = [uuid, email];

                return db.queryAsync(query, queryData).then(() => {
                    return uuid;
                });
            })
            .catch((err) => {
                logger.error(err);
                throw new exception(err);
            });
    }

    checkPaswordReset(uuid) {
        let query = 'SELECT * FROM password_reset WHERE req_uuid = ? LIMIT 1';
        let queryData = [uuid];

        return db.queryAsync(query, queryData).then((response) => {
            if (response.length) return response[0];

            throw new exception(exceptionCodes.EMPTY_DB_RESULT);
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    setNewPassword(data) {
        let query = 'UPDATE users SET password = ? WHERE email = ?';
        let queryData = [data.password, data.email];

        return db.queryAsync(query, queryData)
            // Check if new password is set
            .then((response) => {
                if (response.affectedRows) return true;

                throw new exception(exceptionCodes.EMPTY_DB_RESULT);
            })
            // Delete row from password_reset table
            .then(() => {
                let query = 'DELETE FROM password_reset WHERE req_uuid = ?';
                let queryData = [data.uuid];

                return db.queryAsync(query, queryData);
            })
            .catch((err) => {
                logger.error(err);
                throw new exception(err);
            });
    }
}

module.exports = new PasswordService();