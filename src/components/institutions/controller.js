const emailer = require('../../common/emailer/emailer');
const institutionsService = require('./service');
const Institution = require('../../model/institution');
const User = require('../../model/user');

class InstitutionsController {
    constructor() { }

    // Create institution
    create(req, res) {
        institutionsService.create(req.body).then((result) => {
            // WARNING: Disable emailer in test phase (settings.js)
            // Inform user about registration
            emailer.sendRegistrationMessage(req.body.email);

            res.status(200).send(result);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    // Get list of all institutions
    list(req, res) {
        institutionsService.list().then((results) => {
            let institutions = results[0].map((row) => {
                let userRow = results[1].find((r) => {
                    return r.user_institution_id === row.id;
                });

                let institution = new Institution(row);
                institution.user = new User(userRow);
                return institution;
            });

            res.status(200).send(institutions);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    // Update institution
    update(req, res) {
        institutionsService.update(req.body).then((result) => {
            res.status(200).send(result);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    userList(req, res) {
        institutionsService.userList(req.query).then((results) => {
            let institution = new Institution(results[0]);
            res.status(200).send(institution);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }
}

module.exports = new InstitutionsController();