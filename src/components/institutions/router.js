const express = require('express');
const router = express.Router();
const institutionsController = require('./controller');

router.get('/list', (req, res) => {
    institutionsController.list(req, res);
});

router.post('/create', (req, res) => {
    institutionsController.create(req, res);
});

router.put('/update', (req, res) => {
    institutionsController.update(req, res);
});

router.get('/user/list', (req, res) => {
    institutionsController.userList(req, res);
});

module.exports = router;