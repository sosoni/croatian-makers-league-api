const md5 = require('md5');
const P = require('bluebird');

const db = require('../../db');
const logger = require('../../logger');
const exception = require('../../common/exceptions/exceptions');
const emailer = require('../../common/emailer/emailer');

class InstitutionsService {
    constructor() { }

    // Create institution
    create(data) {
        let userData = [data.email, data.password];
        let userQuery = 'INSERT INTO users VALUES (DEFAULT, DEFAULT, DEFAULT, ?, ?, DEFAULT)';
        let institutionData = [data.name, data.address, data.postcode, data.city, data.country, data.oib, data.director, data.directorEmail, data.directorPhone.split(' ').join(''), data.isInterestedInOrganisation];
        let institutionQuery = 'INSERT INTO institutions VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, DEFAULT, DEFAULT)';
        let userId, institutionId;

        return db.queryAsync(userQuery, userData).then((response) => {
            userId = parseInt(response.insertId, 10);

            return db.queryAsync(institutionQuery, institutionData).then((response) => {
                institutionId = parseInt(response.insertId, 10);
                return response;
            });
        }).then((response) => {
            // Update institutionId in users table
            return db.queryAsync('UPDATE users SET user_institution_id = ? WHERE id = ?', [institutionId, userId])
                .then((resposne) => {
                    return response;
                });
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    // Get list of all institutions
    list() {
        let getUser = db.queryAsync('SELECT * FROM users');
        let getInstitutions = db.queryAsync('SELECT * FROM institutions');

        return P.all([getInstitutions, getUser]).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    // Update institution
    update(data) {
        let getUser = db.queryAsync('SELECT * FROM users WHERE user_institution_id=? LIMIT 1', [data.id]);
        let updateUserAuthorization = db.queryAsync('UPDATE users SET authorized=? WHERE user_institution_id=?', [data.user.authorized ? 1 : 0, data.id]);

        let regionalCenterName = data.isRegionalCenter ? (data.name + ' (' + data.city + ')') : data.regionalCenterName;
        let queryData = [data.name, data.address, data.postcode, data.oib, data.city, regionalCenterName, data.isInterestedInOrganisation ? 1 : 0, data.isRegionalCenter ? 1 : 0, data.id];
        let query = 'UPDATE institutions SET name = ?, address = ?, postcode = ?, oib = ?, city = ?, regional_center_name = ?, interested_in_organisation = ?, regional_center = ? WHERE id = ?';

        // Check if user authorization is changed
        // If it is, update users table and send mail
        return getUser.then((response) => {
            let isAuthorized = !!response[0].authorized;
            if (isAuthorized === data.user.authorized) return false;

            // Authorize/unauthorize user and send mail
            return updateUserAuthorization.then((response) => {
                let email = data.user.email;

                // WARNING: Disable emailer in test phase (settings.js)
                if (!data.user.authorized) emailer.sendUnauthorizedMessage(email); // Unauthorization mail
                else emailer.sendAuthorizedMessage(email); // Authorization mail

                return response;
            });
        }).then((response) => {
            // Update institutions table
            return db.queryAsync(query, queryData).then((response) => {
                return response;
            });
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    userList(data) {
        let query = 'SELECT * FROM institutions WHERE id=? LIMIT 1';
        let queryData = [data.institutionId];


        return db.queryAsync(query, queryData).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }
}

module.exports = new InstitutionsService();