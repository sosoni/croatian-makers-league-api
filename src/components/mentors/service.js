const P = require('bluebird');

const db = require('../../db');
const logger = require('../../logger');
const exception = require('../../common/exceptions/exceptions');

class MentorsService {
    constructor() { }

    // Get list of mentors (and institutions)
    adminList() {
        let getMentors = db.queryAsync('SELECT * FROM mentors');
        let getInstitutions = db.queryAsync('SELECT id, name, oib, interested_in_organisation, regional_center, regional_center_name FROM institutions');

        return P.all([getMentors, getInstitutions]).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    // Update mentors and institution
    adminUpdate(data) {
        // Buil query (bulk update)
        let query = 'INSERT INTO mentors (id, institution_id, name, email, phone, mentors_age_group_id) VALUES ';
        data.forEach(function (mentor) {
            query = query + '(';
            query = query + mentor.id + ',';
            query = query + mentor.institutionId + ',';
            query = query + '"' + mentor.name + '",';
            query = query + '"' + mentor.email + '",';
            query = query + '"' + mentor.phone + '",';
            query = query + (mentor.hasOwnProperty('ageGroup') ? '"' + mentor.ageGroup.id + '"' : null);
            query = query + '),';
        });
        query = query.slice(0, -1);
        query = query + ' ON DUPLICATE KEY UPDATE institution_id=VALUES(institution_id), name=VALUES(name), email=VALUES(email), phone=VALUES(phone), mentors_age_group_id=VALUES(mentors_age_group_id);';

        return db.queryAsync(query).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    userList(queryData) {
        let query = 'SELECT * FROM mentors WHERE institution_id = ' + queryData.institutionId;
        return db.queryAsync(query).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    userUpdate(data) {
        let queryData = [data.name, data.phone, data.email, data.ageGroupId, data.id];
        let query = 'UPDATE mentors SET name = ?, phone = ?, email = ?, mentors_age_group_id = ? WHERE id = ?';

        return db.queryAsync(query, queryData).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    userCreate(data) {
        let queryData = [data.name, data.phone, data.email, data.institutionId, data.ageGroupId];
        let query = 'INSERT INTO mentors SET name = ?, phone = ?, email = ?, institution_id = ?, mentors_age_group_id = ?';

        return db.queryAsync(query, queryData).then((response) => {
            let mentorId = parseInt(response.insertId, 10);
            return {
                mentorId: mentorId
            };
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }
}

module.exports = new MentorsService();