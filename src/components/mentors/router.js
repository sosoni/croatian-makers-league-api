const express = require('express');
const router = express.Router();
const mentorsController = require('./controller');

router.get('/admin/list', (req, res) => {
    mentorsController.adminList(req, res);
});

router.put('/admin/update', (req, res) => {
    mentorsController.adminUpdate(req, res);
});

router.get('/user/list', (req, res) => {
    mentorsController.userList(req, res);
});

router.put('/user/update', (req, res) => {
    mentorsController.userUpdate(req, res);
});

router.post('/user/create', (req, res) => {
    mentorsController.userCreate(req, res);
});

module.exports = router;