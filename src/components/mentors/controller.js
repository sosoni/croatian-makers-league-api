const mentorsService = require('./service');

const Mentor = require('../../model/mentor');
const mentorsAgeGroups = require('../../model/mentorsAgeGroups');
const Institution = require('../../model/institution');

class MentorsController {
    constructor() { }

    // Get list of all mentors
    adminList(req, res) {
        mentorsService.adminList().then((response) => {
            let data = {
                mentors: [],
                mentorsAgeGroups: mentorsAgeGroups,
                institutions: []
            };

            // Mentors
            response[0].forEach((row) => {
                data.mentors.push(new Mentor(row));
            });

            // Institutions
            response[1].forEach((row) => {
                data.institutions.push(new Institution(row));
            });

            res.status(200).send(data);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    // Update mentors
    adminUpdate(req, res) {
        mentorsService.adminUpdate(req.body).then((response) => {
            res.status(200).send('Mentors updated successefully.');
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    userList(req, res) {
        mentorsService.userList(req.query).then((response) => {
            let data = {
                mentors: [],
                mentorsAgeGroups: mentorsAgeGroups
            };

            response.forEach((row) => {
                data.mentors.push(new Mentor(row));
            });

            res.status(200).send(data);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    userUpdate(req, res) {
        mentorsService.userUpdate(req.body).then((response) => {
            res.status(200).send('Mentors updated successefully.');
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    userCreate(req, res) {
        mentorsService.userCreate(req.body).then((response) => {
            res.status(200).send(response);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }
}

module.exports = new MentorsController();