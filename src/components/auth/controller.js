const authService = require('./service');
const User = require('../../model/user');

class AuthController {

    constructor() { }

    login(req, res) {
        authService.login(req).then((loginRes) => {
            let user = new User(loginRes);
            let token = authService.signUserToken(user);

            if (token) res.set('Access-Token', token).status(200).json(user);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    loginCheck(req, res) {
        let authHeader = req.get('Authorization');

        authService.loginCheck(authHeader).then((loginRes) => {
            let user = new User(loginRes);
            let token = authService.signUserToken(user);

            if (token) res.set('Access-Token', token).status(200).json(user);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    logout(req, res) {
        // Maybe invalidate token on this req in some happier times
        res.sendStatus(200);
    }
}

module.exports = new AuthController();