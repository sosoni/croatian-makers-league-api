const express = require('express');
const router = express.Router();

const authController = require('./controller');

router.post('/login', (req, res) => {
    authController.login(req, res);
});

router.get('/login_check', (req, res) => {
    authController.loginCheck(req, res);
});

router.get('/logout', (req, res) => {
    authController.logout(req, res);
});

module.exports = router;