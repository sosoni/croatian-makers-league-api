const jwt = require('jsonwebtoken');
const md5 = require('md5');

const settings = require('../../../settings');
const db = require('../../db');
const logger = require('../../logger');
const exception = require('../../common/exceptions/exceptions');
const exceptionCodes = require('../../common/exceptions/exception-codes');

class AppService {
    constructor() { }

    login(req) {
        let query = 'SELECT * FROM users WHERE email = ? LIMIT 1';
        let queryData = [req.body.email];

        return db.queryAsync(query, queryData).then((response) => {
            if (response.length) {
                let user = response[0];

                if (!user.authorized) throw new exception(exceptionCodes.UNAUTHORIZED);
                if (user.password === req.body.password) return user;
            }

            throw new exception(exceptionCodes.EMPTY_DB_RESULT);
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    loginCheck(authHeader) {
        return jwt.verifyAsync(authHeader, 'cmleague').then((result) => {
            let query = 'SELECT * FROM users WHERE email = ? LIMIT 1';
            let queryData = [result.email];

            return db.queryAsync(query, queryData).then((response) => {
                if (response.length) {
                    let user = response[0];

                    if (!user.authorized) throw new exception(exceptionCodes.UNAUTHORIZED);
                    return user;
                }

                throw new exception(exceptionCodes.EMPTY_DB_RESULT);
            });
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    signUserToken(data) {
        let tokenOptions = { expiresIn: settings.jwt.tokenDuration };
        let tokenData = { email: data.email };
        let token = jwt.sign(tokenData, settings.jwt.secretPassPhrase, tokenOptions);

        return token;
    }
}

module.exports = new AppService();