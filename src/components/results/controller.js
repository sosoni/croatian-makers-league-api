const resultsService = require('./service');

class ResultsController {
    constructor() { }

    list(req, res) {
        resultsService.list().then((result) => {
            res.status(200).send(result);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    create(req, res) {
        resultsService.create(req.body).then((result) => {
            res.status(200).send(result);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    update (req, res) {
        resultsService.update(req.body).then((result) => {
            res.status(200).send(result);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }
}

module.exports = new ResultsController();
