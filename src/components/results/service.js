const db = require('../../db');
const logger = require('../../logger');
const exception = require('../../common/exceptions/exceptions');

class ResultsService {
    constructor() { }

    list() {
        let query = 'SELECT * FROM results';

        return db.queryAsync(query).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    create(data) {
        let queryData = [data.name, data.link];
        let query = 'INSERT INTO results VALUES (DEFAULT, ?, ?, DEFAULT)';

        return db.queryAsync(query, queryData).then((response) => {
            let resultId = parseInt(response.insertId, 10);

            return { resultId: resultId };
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    update (data) {
        let queryData = [data.name, data.link, data.id];
        let query = 'UPDATE results SET name = ?, link = ? WHERE id = ?';

        return db.queryAsync(query, queryData).then((response) => {
            let resultId = parseInt(response.insertId, 10);

            return { resultId: resultId };
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }
}

module.exports = new ResultsService();
