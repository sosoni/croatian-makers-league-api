const express = require('express');
const router = express.Router();
const resultsController = require('./controller');

router.get('/list', (req, res) => {
    resultsController.list(req, res);
});

router.put('/update', (req, res) => {
    resultsController.update(req, res);
});

router.post('/create', (req, res) => {
    resultsController.create(req, res);
});

module.exports = router;
