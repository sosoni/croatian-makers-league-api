const P = require('bluebird');

const db = require('../../db');
const logger = require('../../logger');
const exception = require('../../common/exceptions/exceptions');

class RoundsService {
    constructor() { }

    // Create round with result names
    adminCreate(data) {
        // Rounds query
        let queryData = [data.type.id, data.name, new Date(data.created), data.opened ? 1 : 0, data.googleSheets, data.googleSheets2];
        let query = 'INSERT INTO rounds VALUES(DEFAULT, ?, ?, ?, ?, ?, ?)';

        return db.queryAsync(query, queryData)
            .then((response) => {
                let roundId = parseInt(response.insertId, 10);

                return {
                    roundId: roundId
                };
            })
            .catch((err) => {
                logger.error(err);
                throw new exception(err);
            });
    }

    // Get list of rounds
    adminList() {
        let query = 'SELECT * FROM rounds';

        return db.queryAsync(query)
            .then((response) => {
                return response;
            }).catch((err) => {
                logger.error(err);
                throw new exception(err);
            });
    }

    // Update round
    adminUpdate(data) {
        let queryDataRounds = [data.name, data.opened, data.googleSheets, data.googleSheets2, data.id];
        let queryRounds = 'UPDATE rounds SET name = ?, opened = ?, google_sheets = ?, google_sheets2 = ? WHERE id = ?';

        // Update
        return db.queryAsync(queryRounds, queryDataRounds)
            .then((response) => {
                return response;
            }).catch((err) => {
                logger.error(err);
                throw new exception(err);
            });
    }

    userList(queryData) {
        let getCompetitors = db.queryAsync('SELECT * FROM competitors WHERE institution_id = ' + queryData.institutionId);
        let getMentors = db.queryAsync('SELECT * FROM mentors WHERE institution_id = ' + queryData.institutionId);
        let getRounds = db.queryAsync('SELECT * FROM rounds');
        let getRoundsMap = db.queryAsync('SELECT * FROM rounds_map WHERE institution_id = ' + queryData.institutionId);
        let getRegionalCenterNames = db.queryAsync('SELECT regional_center_name FROM institutions WHERE regional_center=1');
        let getInstitution = db.queryAsync('SELECT * FROM institutions WHERE id = ' + queryData.institutionId + ' LIMIT 1');

        return P.all([getCompetitors, getMentors, getRounds, getRoundsMap, getRegionalCenterNames, getInstitution]).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    userUpdate(data) {
        let queryData = [
            data.roundId,
            data.institutionId,
            data.mentors[0] ? data.mentors[0].id : null,
            data.mentors[0] ? data.mentors[0].compeatingGroup : null,
            data.mentors[1] ? data.mentors[1].id : null,
            data.mentors[1] ? data.mentors[1].compeatingGroup : null,
            data.competitors[0] ? data.competitors[0].id : null,
            data.competitors[0] ? data.competitors[0].compeatingGroup : null,
            data.competitors[1] ? data.competitors[1].id : null,
            data.competitors[1] ? data.competitors[1].compeatingGroup : null,
            data.competitors[2] ? data.competitors[2].id : null,
            data.competitors[2] ? data.competitors[2].compeatingGroup : null,
            data.competitors[3] ? data.competitors[3].id : null,
            data.competitors[3] ? data.competitors[3].compeatingGroup : null
        ];
        let query = 'INSERT INTO rounds_map VALUES(DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

        // Update institution regional center
        db.queryAsync('UPDATE institutions SET regional_center_name=? WHERE id=?', [data.regionalCenterName, data.institutionId])
            .then((response) => { })
            .catch((err) => {
                logger.error(err);
                throw new exception(err);
            });

        return db.queryAsync('DELETE FROM rounds_map WHERE round_id=? AND institution_id=?', [data.roundId, data.institutionId])
            .then((response) => {
                return db.queryAsync(query, queryData).then((response) => {
                    return response;
                });
            })
            .catch((err) => {
                logger.error(err);
                throw new exception(err);
            });
    }
}

module.exports = new RoundsService();
