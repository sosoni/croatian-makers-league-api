const express = require('express');
const router = express.Router();
const roundsController = require('./controller');

router.post('/admin/create', (req, res) => {
    roundsController.adminCreate(req, res);
});

router.get('/admin/list', (req, res) => {
    roundsController.adminList(req, res);
});

router.put('/admin/update', (req, res) => {
    roundsController.adminUpdate(req, res);
});

router.get('/user/list', (req, res) => {
    roundsController.userList(req, res);
});

router.put('/user/update', (req, res) => {
    roundsController.userUpdate(req, res);
});

module.exports = router;
