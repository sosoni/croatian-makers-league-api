const roundsService = require('./service');
const Institution = require('../../model/institution');
const Competitor = require('../../model/competitor');
const Mentor = require('../../model/mentor');
const Round = require('../../model/round');
const RoundsMap = require('../../model/roundsMap');
const roundsTypes = require('../../model/roundsTypes');

class RoundsController {
    constructor() { }

    // Create round
    adminCreate(req, res) {
        roundsService.adminCreate(req.body).then((result) => {
            res.status(200).send(result);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    // Get list of all rounds
    adminList(req, res) {
        roundsService.adminList().then((result) => {
            let data = {
                rounds: [],
                roundsTypes: roundsTypes
            };

            // Rounds
            result.forEach((row) => {
                data.rounds.push(new Round(row));
            });

            res.status(200).send(data);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    // Update round
    adminUpdate(req, res) {
        roundsService.adminUpdate(req.body).then((response) => {
            res.status(200).send('Round updated successefully.');
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    userList(req, res) {
        roundsService.userList(req.query).then((response) => {
            let data = {
                competitors: [],
                mentors: [],
                rounds: [],
                roundsMap: [],
                roundsTypes: roundsTypes,
                regionalCenterNames: [],
                institution: null
            };

            // Competitors
            response[0].forEach((row) => {
                data.competitors.push(new Competitor(row));
            });

            // Mentors
            response[1].forEach((row) => {
                data.mentors.push(new Mentor(row));
            });

            // Rounds
            response[2].forEach((row) => {
                data.rounds.push(new Round(row));
            });

            // Rounds map
            response[3].forEach((row) => {
                data.roundsMap.push(new RoundsMap(row));
            });

            // Regional center names
            response[4].forEach((row) => {
                data.regionalCenterNames.push(new Institution(row));
            });

            // Institution
            data.institution = new Institution(response[5][0]);

            res.status(200).send(data);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    userUpdate(req, res) {
        roundsService.userUpdate(req.body).then((response) => {
            res.status(200).send('Competitors updated successefully.');
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }
}

module.exports = new RoundsController();
