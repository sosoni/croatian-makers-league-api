const express = require('express');
const router = express.Router();
const competitorsController = require('./controller');

router.get('/admin/list', (req, res) => {
    competitorsController.adminList(req, res);
});

router.put('/admin/update', (req, res) => {
    competitorsController.adminUpdate(req, res);
});

router.get('/user/list', (req, res) => {
    competitorsController.userList(req, res);
});

router.put('/user/update', (req, res) => {
    competitorsController.userUpdate(req, res);
});

router.post('/user/create', (req, res) => {
    competitorsController.userCreate(req, res);
});

router.get('/download/:documentName', (req, res) => {
    competitorsController.downloadDocument(req, res);
});

module.exports = router;