const competitorsService = require('./service');
const Competitor = require('../../model/competitor');
const competitorsAgeGroups = require('../../model/competitorsAgeGroups');
const Mentor = require('../../model/mentor');
const Institution = require('../../model/institution');
const exception = require('../../common/exceptions/exceptions');
const logger = require('../../logger');

class CompetitorsController {
    constructor() { }

    // Get list of all competitors
    adminList(req, res) {
        competitorsService.adminList().then((response) => {
            let data = {
                competitors: [],
                competitorsAgeGroups: competitorsAgeGroups,
                institutions: [],
                mentors: []
            };

            // Competitors
            response[0].forEach((row) => {
                data.competitors.push(new Competitor(row));
            });

            // Institutions
            response[1].forEach((row) => {
                data.institutions.push(new Institution(row));
            });

            // Mentors
            response[2].forEach((row) => {
                data.mentors.push(new Mentor(row));
            });

            res.status(200).send(data);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    // Update competitors
    adminUpdate(req, res) {
        competitorsService.adminUpdate(req.body).then((response) => {
            res.status(200).send('Competitor updated successefully.');
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    downloadDocument(req, res) {
        let file = cml.path + '/upload/documents/' + req.params.documentName;
        res.set('Content-Disposition', req.params.documentName);
        res.download(file, req.params.documentName);
    }

    userList(req, res) {
        competitorsService.userList(req.query).then((response) => {
            let data = {
                competitors: [],
                competitorsAgeGroups: competitorsAgeGroups
            };

            response.forEach((row) => {
                data.competitors.push(new Competitor(row));
            });

            res.status(200).send(data);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    userUpdate(req, res) {
        competitorsService.userUpdate(req.body).then((response) => {
            res.status(200).send('Competitor updated successefully.');
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }

    userCreate(req, res) {
        competitorsService.userCreate(req.body).then((response) => {
            res.status(200).send(response);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }
}

module.exports = new CompetitorsController();