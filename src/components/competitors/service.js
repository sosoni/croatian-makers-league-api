const promise = require('bluebird');
const FileParser = require('busboy');
const fs = require('fs');
const path = require('path');

const db = require('../../db');
const logger = require('../../logger');
const exception = require('../../common/exceptions/exceptions');
const exceptionCodes = require('../../common/exceptions/exception-codes');
const settings = require('../../../settings');

class MentorsService {
    constructor() { }

    // Get list of competitors, institutions, mentors
    adminList() {
        let getCompetitors = db.queryAsync('SELECT * FROM competitors');
        let getInstitutions = db.queryAsync('SELECT id, name, oib, interested_in_organisation, regional_center, regional_center_name FROM institutions');
        let getMentors = db.queryAsync('SELECT * FROM mentors');

        return promise.all([getCompetitors, getInstitutions, getMentors])
            .then((response) => {
                return response;
            }).catch((err) => {
                logger.error(err);
                throw new exception(err);
            });
    }

    // Update competitor
    adminUpdate(data) {
        let queryData = [data.name, data.lastname, new Date(data.birthdate), data.class, data.sex, data.approved ? 1 : 0, data.ageGroup ? data.ageGroup.id : null, data.id];
        let query = 'UPDATE competitors SET name = ?, lastname = ?, birthdate = ?, class = ?, sex = ?, approved = ?, competitors_age_group_id = ? WHERE id = ?';

        return db.queryAsync(query, queryData).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    userList(queryData) {
        let query = 'SELECT * FROM competitors WHERE institution_id = ' + queryData.institutionId;

        return db.queryAsync(query).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    userUpdate(data) {
        let queryData = [data.name, data.lastname, new Date(data.birthdate), data.class, data.sex, data.ageGroupId, data.id];
        let query = 'UPDATE competitors SET name = ?, lastname = ?, birthdate = ?, class = ?, sex = ?, competitors_age_group_id = ? WHERE id = ?';

        return db.queryAsync(query, queryData).then((response) => {
            return response;
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    userCreate(data) {
        let queryData = [data.name, data.lastname, new Date(data.birthdate), data.class, data.sex, data.institutionId, data.ageGroupId];
        let query = 'INSERT INTO competitors SET name = ?, lastname = ?, birthdate = ?, class = ?, sex = ?, institution_id = ?, competitors_age_group_id = ?';

        return db.queryAsync(query, queryData).then((response) => {
            let competitorId = parseInt(response.insertId, 10);
            return {
                competitorId: competitorId
            };
        }).catch((err) => {
            logger.error(err);
            throw new exception(err);
        });
    }

    saveDocument(req, onFinish) {
        let fileParser = new FileParser({
            headers: req.headers,
            limits: {
                files: 1,
                fileSize: settings.fileUpload.fileLimit
            }
        });
        let output = {};

        fileParser.on('file', (fieldName, stream, fileName, encoding, mimeType) => {
            let fileSize = 0;
            let filePath = cml.path + '/upload/documents/' + fileName;

            if (fs.existsSync(filePath)) {
                stream.resume();
                output = new exception(exceptionCodes.FILE_EXISTS);
            } else {
                stream.on('data', function (data) {
                    if (data.length) {
                        fileSize += data.length;
                    }
                });

                stream.on('end', function () {
                    output.approvalDoc = fileName;
                    output.extension = path.extname(fileName);
                    output.encoding = encoding;
                    output.mimeType = mimeType;
                    output.size = fileSize;
                });

                stream.pipe(fs.createWriteStream(cml.path + '/upload/documents/' + fileName));
            }
        });

        fileParser.on('field', function (fieldName, value) {
            output[fieldName] = value;
        });

        fileParser.on('filesLimit', function () {
            output = new exception(exceptionCodes.MAX_FILES_UPLOADS_EXCEEDED);
        });

        fileParser.on('finish', () => {
            onFinish.call(this, output);
        });

        return req.pipe(fileParser);
    }
}

module.exports = new MentorsService();