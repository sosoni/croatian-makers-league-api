let mysql = require('mysql');

// Setup connection object
let dbJSON = {};
try {
    dbJSON = require('../db.json') || {};
} catch (e) {
    console.log('Warning: ', e);
}
let connObj = {
    host: dbJSON.host || 'localhost',
    user: dbJSON.user || 'root',
    password: dbJSON.password || 'root',
    database: dbJSON.database || 'cml'
};

// Connect
let connection = mysql.createConnection(connObj);
connection.connect(function (err) {
    if (!err) {
        console.log('Database is connected!');
    } else {
        console.log('Error connecting database: ', err);
    }
});

module.exports = connection;