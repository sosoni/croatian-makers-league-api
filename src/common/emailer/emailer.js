const Emailer = require('../../model/emailer');
const settings = require('../../../settings');

// Emailer on support
exports.support = function (req, res) {
    let message = req.body;

    // Set sender email at bottom of message
    message.text = message.text + '\n \r' + message.from;

    let email = new Emailer('support');
    email.send(message).then((info) => {
        res.json(info);
    }).catch((err) => {
        res.json(err);
    });
};

// Emailer on regiester
exports.sendRegistrationMessage = function (recipient) {
    let message = {
        to: recipient,
        subject: settings.emailer.content.registration.subject,
        text: settings.emailer.content.registration.text
    };

    // Type of mailer should be changed ('support'); needed new forwarder mail type
    let email = new Emailer('support');
    email.send(message).then((info) => {
        console.log('Registration mail sent!');
    }).catch((err) => {
        console.log('Registration mail not sent!' + err);
    });
};

// Emailer on password reset
exports.sendPasswordReset = function (data) {
    let message = {
        to: data.email,
        subject: settings.emailer.content.passReset.subject,
        text: settings.emailer.content.passReset.text + data.href
    };

    let email = new Emailer('support');
    email.send(message).then((info) => {
        console.log('Password reset mail sent!');
    }).catch((err) => {
        console.log('Password reset mail not sent!' + err);
    });
};

exports.sendUnauthorizedMessage = function (recipient) {
    let message = {
        to: recipient,
        subject: settings.emailer.content.deactivation.subject,
        text: settings.emailer.content.deactivation.text
    };

    let email = new Emailer('support');
    email.send(message).then((info) => {
        console.log('Unauthorization mail sent!');
    }).catch((err) => {
        console.log('Unauthorization mail not sent!' + err);
    });
};

exports.sendAuthorizedMessage = function (recipient) {
    let message = {
        to: recipient,
        subject: settings.emailer.content.activation.subject,
        text: settings.emailer.content.activation.text
    };

    let email = new Emailer('support');
    email.send(message).then((info) => {
        console.log('Authorization mail sent!');
    }).catch((err) => {
        console.log('Authorization mail not sent!' + err);
    });
};