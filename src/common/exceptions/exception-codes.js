// Export
module.exports = {
    UNKNOWN_ERROR: {
        code: 'UNKNOWN_ERROR',
        httpCode: 400
    },
    MAX_FILES_UPLOADS_EXCEEDED: {
        code: 'MAX_FILES_UPLOADS_EXCEEDED',
        httpCode: 400,
        message: 'Max number of files exceeded.'
    },
    MISSING_PARAMS: {
        code: 'MISSING_PARAMS',
        httpCode: 400
    },
    UNAUTHORIZED: {
        code: 'UNAUTHORIZED',
        httpCode: 401,
        message: 'Unauthorized action'
    },
    FORBIDDEN: {
        code: 'FORBIDDEN',
        httpCode: 403
    },
    EMPTY_DB_RESULT: {
        code: 'EMPTY_DB_RESULT',
        httpCode: 404,
        message: 'No entries found for this request.'
    },
    FILE_EXISTS: {
        code: 'FILE_EXISTS',
        httpCode: 404,
        message: 'File with given filename already exists.'
    },
    ER_DUP_ENTRY: {
        code: 'DUPLICATE_DB_RESULT',
        httpCode: 409,
        message: 'This entry already exists.'
    },
    NONEXISTENT_ENTITY: {
        code: 'NONEXISTENT_DB_ROW',
        httpCode: 422,
        message: 'This entity doesn\'t exist.'
    },
    NONEXISTENT_FILE: {
        code: 'NONEXISTENT_FILE',
        httpCode: 404,
        message: 'This file doesn\'t exist.'
    },
    INVALID_AUTH: {
        code: 'INVALID_AUTH',
        httpCode: 403,
        message: 'Authentication failed because of invalid credentials.'
    }
};