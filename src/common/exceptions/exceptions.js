const exceptionCodes = require('./exception-codes');
const _ = require('lodash');

class Exception extends Error {
    constructor(exception, message, details) {
        super();

        if (_.isString(exception)) {
            this.code = exception;
            exception = exceptionCodes[exception] || exceptionCodes.UNKNOWN_ERROR;
        }

        this.name = this.constructor.name;
        this.httpCode = exception && exception.httpCode ? exception.httpCode : 418;
        this.code = this.code ? this.code : exception && exception.code ? exception.code : 'UNKNOWN_ERROR';
        this.message = message ? message : exception && exception.message ? exception.message : 'Unknown error';
        this.details = details ? details : [];

        // Check stack trace
        if (typeof Error.captureStackTrace === 'function') {
            Error.captureStackTrace(this, this.constructor);
        } else {
            this.stack = (new Error(message)).stack;
        }
    }

    static unhandledError(response) {
        return new Exception('UNKNOWN_ERROR', response.code);
    }
}

// Export
module.exports = Exception;