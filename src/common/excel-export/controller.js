const excel = require('node-excel-export');
const exception = require('../exceptions/exceptions');

const exportService = require('./service');

let buildExport = (specification, data) => {
    return excel.buildExport(
        [{
            specification: specification,
            data: data
        }]
    );
};

class ExportController {
    constructor() { }

    export(req, res) {
        exportService.export(req).then((results) => {
            let Model, data, specification, buildExp;

            switch (req.query.type) {
                case 'institutions':
                    Model = require('./model/institutions/model');
                    data = [];

                    results[0].forEach((row) => {
                        let userData = results[1].find((row2) => {
                            return row2.user_institution_id === row.id;
                        });
                        data.push(new Model(row, userData));
                    });

                    specification = require('./model/institutions/specification');
                    buildExp = buildExport(specification, data);
                    break;
                case 'mentors':
                    specification = require('./model/mentors/specification');
                    buildExp = buildExport(specification, results[0]);
                    break;
                case 'competitors':
                    specification = require('./model/competitors/specification');
                    buildExp = buildExport(specification, results[0]);
                    break;
                case 'rounds':
                    Model = require('./model/rounds/model');
                    data = [];

                    let roundsMap = results[0];
                    let round = results[1][0];
                    let institutions = results[2];
                    let mentors = results[3];
                    let competitors = results[4];
                    let users = results[5];

                    roundsMap.forEach((roundMap) => {
                        let institution = institutions.find((inst) => {
                            return inst.id === roundMap.institution_id;
                        });

                        let mentor1 = mentors.find((mentor) => {
                            return mentor.id === roundMap.mentor1_id;
                        }) || {};

                        let mentor2 = mentors.find((mentor) => {
                            return mentor.id === roundMap.mentor2_id;
                        }) || {};

                        let competitor1 = competitors.find((competitor) => {
                            return competitor.id === roundMap.competitor1_id;
                        }) || {};

                        let competitor2 = competitors.find((competitor) => {
                            return competitor.id === roundMap.competitor2_id;
                        }) || {};

                        let competitor3 = competitors.find((competitor) => {
                            return competitor.id === roundMap.competitor3_id;
                        }) || {};

                        let competitor4 = competitors.find((competitor) => {
                            return competitor.id === roundMap.competitor4_id;
                        }) || {};

                        let user = users.find((us) => {
                            return us.user_institution_id === roundMap.institution_id;
                        }) || {};

                        data.push(new Model(round, institution, mentor1, mentor2, competitor1, competitor2, competitor3, competitor4, user));
                    });

                    specification = require('./model/rounds/specification');
                    buildExp = buildExport(specification, data);
                    break;
            }

            if (buildExp) return res.send(buildExp);
        }).catch((err) => {
            res.status(err.httpCode).send(err);
        });
    }
}

module.exports = new ExportController();