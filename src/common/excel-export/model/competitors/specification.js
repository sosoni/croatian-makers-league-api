module.exports = {
    institution_id: {
        displayName: 'Institution ID',
        headerStyle: { font: { bold: true } },
        width: '8'
    },
    id: {
        displayName: 'ID',
        headerStyle: { font: { bold: true } },
        width: '4'
    },
    name: {
        displayName: 'Ime',
        headerStyle: { font: { bold: true } },
        width: 50
    },
    lastname: {
        displayName: 'Prezime',
        headerStyle: { font: { bold: true } },
        width: 50
    },
    birthdate: {
        displayName: 'Datum rođenja',
        headerStyle: { font: { bold: true } },
        width: 100
    },
    class: {
        displayName: 'Razred',
        headerStyle: { font: { bold: true } },
        width: '5'
    },
    sex: {
        displayName: 'Spol',
        headerStyle: { font: { bold: true } },
        width: '5'
    },
    competitors_age_group_id: {
        displayName: 'Grupa',
        headerStyle: { font: { bold: true } },
        width: 60,
        cellFormat: (val, row) => {
            let group = '-';

            if (val === 1) group = 'Mlađi';
            else if (val === 2) group = 'Stariji';
            else if (val === 3) group = 'Svi';

            return group;
        }
    },
};