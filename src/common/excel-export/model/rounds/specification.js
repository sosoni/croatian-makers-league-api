module.exports = {
    userEmail: {
        displayName: 'Korisnički email',
        headerStyle: { font: { bold: true } },
        width: 150
    },
    id: {
        displayName: 'ID kola',
        headerStyle: { font: { bold: true } },
        width: '4'
    },
    type: {
        displayName: 'Tip kola',
        headerStyle: { font: { bold: true } },
        width: '7'
    },
    name: {
        displayName: 'Naziv kola',
        headerStyle: { font: { bold: true } },
        width: 160
    },
    created: {
        displayName: 'Napravljen',
        headerStyle: { font: { bold: true } },
        width: 80
    },
    sheets: {
        displayName: 'Raspored',
        headerStyle: { font: { bold: true } },
        width: 120
    },
    sheets2: {
        displayName: 'Bodovanje',
        headerStyle: { font: { bold: true } },
        width: 120
    },
    institutionId: {
        displayName: 'ID institucije',
        headerStyle: { font: { bold: true } },
        width: '10'
    },
    institutionName: {
        displayName: 'Naziv institucije',
        headerStyle: { font: { bold: true } },
        width: 150
    },
    address: {
        displayName: 'Adresa institucije',
        headerStyle: { font: { bold: true } },
        width: 150
    },
    oib: {
        displayName: 'OIB institucije',
        headerStyle: { font: { bold: true } },
        width: 120
    },
    regionalCenterName: {
        displayName: 'Naziv regionalnog centra',
        headerStyle: { font: { bold: true } },
        width: 150
    },
    mentor1Id: {
        displayName: 'ID mlađeg mentora',
        headerStyle: { font: { bold: true } },
        width: '10'
    },
    mentor1Name: {
        displayName: 'Naziv mlađeg mentora',
        headerStyle: { font: { bold: true } },
        width: 120
    },
    mentor1Email: {
        displayName: 'Email mlađeg mentora',
        headerStyle: { font: { bold: true } },
        width: 120
    },
    mentor2Id: {
        displayName: 'ID starijeg mentora',
        headerStyle: { font: { bold: true } },
        width: '10'
    },
    mentor2Name: {
        displayName: 'Naziv starijeg mentora',
        headerStyle: { font: { bold: true } },
        width: 120
    },
    mentor2Email: {
        displayName: 'Email starijeg mentora',
        headerStyle: { font: { bold: true } },
        width: 120
    },
    competitor1Id: {
        displayName: 'Id 1.mlađeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: '10'
    },
    competitor1Name: {
        displayName: 'Ime 1.mlađeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor1Lastname: {
        displayName: 'Prezime 1.mlađeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor1Birthdate: {
        displayName: 'Datum rođenja 1.mlađeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor1Class: {
        displayName: 'Razred 1.mlađeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor2Id: {
        displayName: 'Id 2.mlađeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: '10'
    },
    competitor2Name: {
        displayName: 'Ime 2.mlađeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor2Lastname: {
        displayName: 'Prezime 2.mlađeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor2Birthdate: {
        displayName: 'Datum rođenja 2.mlađeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor2Class: {
        displayName: 'Razred 2.mlađeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor3Id: {
        displayName: 'Id 1.starijeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: '10'
    },
    competitor3Name: {
        displayName: 'Ime 1.starijeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor3Lastname: {
        displayName: 'Prezime 1.starijeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor3Birthdate: {
        displayName: 'Datum rođenja 1.starijeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor3Class: {
        displayName: 'Razred 1.starijeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor4Id: {
        displayName: 'Id 2.starijeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: '10'
    },
    competitor4Name: {
        displayName: 'Ime 2.starijeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor4Lastname: {
        displayName: 'Prezime 2.starijeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor4Birthdate: {
        displayName: 'Datum rođenja 2.starijeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    competitor4Class: {
        displayName: 'Razred 2.starijeg natjecatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    }
};