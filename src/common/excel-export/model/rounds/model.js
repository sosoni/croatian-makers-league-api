class Round {
    constructor(round, institution, mentor1, mentor2, competitor1, competitor2, competitor3, competitor4, user) {
        this.userEmail = user.email;

        // Round
        this.id = round.id;
        this.type = (round.round_type_id === 1) ? 'Online' : (round.round_type_id === 2) ? 'Fizičko' : '';
        this.name = round.name;
        this.created = round.created;
        this.sheets = round.google_sheets;
        this.sheets2 = round.google_sheets2;

        // Institution
        this.institutionId = institution.id;
        this.institutionName = institution.name;
        this.address = institution.city + ', ' + institution.address;
        this.oib = institution.oib;
        this.regionalCenterName = institution.regional_center_name;

        // Mentors
        this.mentor1Id = mentor1.id;
        this.mentor1Name = mentor1.name;
        this.mentor1Email = mentor1.email;
        this.mentor2Id = mentor2.id;
        this.mentor2Name = mentor2.name;
        this.mentor2Email = mentor2.email;

        // Competitors
        this.competitor1Id = competitor1.id;
        this.competitor1Name = competitor1.name;
        this.competitor1Lastname = competitor1.lastname;
        this.competitor1Birthdate = competitor1.birthdate;
        this.competitor1Class = competitor1.class;
        this.competitor2Id = competitor2.id;
        this.competitor2Name = competitor2.name;
        this.competitor2Lastname = competitor2.lastname;
        this.competitor2Birthdate = competitor2.birthdate;
        this.competitor2Class = competitor2.class;
        this.competitor3Id = competitor3.id;
        this.competitor3Name = competitor3.name;
        this.competitor3Lastname = competitor3.lastname;
        this.competitor3Birthdate = competitor3.birthdate;
        this.competitor3Class = competitor3.class;
        this.competitor4Id = competitor4.id;
        this.competitor4Name = competitor4.name;
        this.competitor4Lastname = competitor4.lastname;
        this.competitor4Birthdate = competitor4.birthdate;
        this.competitor4Class = competitor4.class;
    }
}

module.exports = Round;