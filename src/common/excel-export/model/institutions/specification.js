module.exports = {
    id: {
        displayName: 'ID',
        headerStyle: { font: { bold: true } },
        width: '4'
    },
    userEmail: {
        displayName: 'Korisnički email',
        headerStyle: { font: { bold: true } },
        width: 160
    },
    name: {
        displayName: 'Naziv institucije',
        headerStyle: { font: { bold: true } },
        width: 160
    },
    address: {
        displayName: 'Adresa',
        headerStyle: { font: { bold: true } },
        width: 200
    },
    oib: {
        displayName: 'OIB',
        headerStyle: { font: { bold: true } },
        width: 140
    },
    director: {
        displayName: 'Ravnatelj',
        headerStyle: { font: { bold: true } },
        width: 130
    },
    directorEmail: {
        displayName: 'Email ravnatelja',
        headerStyle: { font: { bold: true } },
        width: 150
    },
    directorPhone: {
        displayName: 'Telefon ravnatelja',
        headerStyle: { font: { bold: true } },
        width: 70
    },
    interestedInOrganisation: {
        displayName: 'Zainteresiran za organizaciju kola',
        headerStyle: { font: { bold: true } },
        width: '10'
    },
    regionalCenter: {
        displayName: 'Regionalni centar',
        headerStyle: { font: { bold: true } },
        width: '12'
    },
    regionalCenterName: {
        displayName: 'Ime regionalnog centra',
        headerStyle: { font: { bold: true } },
        width: 250
    }
};