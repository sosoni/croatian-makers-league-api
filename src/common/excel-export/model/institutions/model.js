class Institution {
    constructor (institution, user) {
        this.id = institution.id;
        this.userEmail = user.email;
        this.name = institution.name;
        this.address = institution.city + ', ' + institution.address;
        this.oib = institution.oib;
        this.director = institution.director;
        this.directorEmail = institution.director_email;
        this.directorPhone = institution.director_phone;
        this.interestedInOrganisation = (institution.interestedInOrganisation === 1) ? 'DA' : 'NE';
        this.regionalCenter = (institution.regional_center === 1) ? 'DA' : 'NE';
        this.regionalCenterName = institution.regional_center_name;
    }
}

module.exports = Institution;