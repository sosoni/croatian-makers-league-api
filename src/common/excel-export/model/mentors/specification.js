module.exports = {
    institution_id: {
        displayName: 'Institution ID',
        headerStyle: { font: { bold: true } },
        width: '8'
    },
    id: {
        displayName: 'ID',
        headerStyle: { font: { bold: true } },
        width: '4'
    },
    name: {
        displayName: 'Ime',
        headerStyle: { font: { bold: true } },
        width: 150
    },
    email: {
        displayName: 'Email',
        headerStyle: { font: { bold: true } },
        width: 200
    },
    phone: {
        displayName: 'Telefon',
        headerStyle: { font: { bold: true } },
        width: 130
    },
    mentors_age_group_id: {
        displayName: 'Grupa',
        headerStyle: { font: { bold: true } },
        width: 150,
        cellFormat: (val, row) => {
            let group = '-';

            if (val === 1) group = 'Mlađi';
            else if (val === 2) group = 'Stariji';
            else if (val === 3) group = 'Svi';
            
            return group;
        }
    }
};