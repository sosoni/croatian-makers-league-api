const express = require('express');
const router = express.Router();
const controller = require('./controller');

router.get('/export', (req, res) => {
    controller.export(req, res);
});

module.exports = router;