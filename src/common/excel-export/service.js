
const P = require('bluebird');
const db = require('../../db');
const logger = require('../../logger');
const exception = require('../exceptions/exceptions');

class ExportService {
    constructor() { }

    export(req) {
        let type = req.query.type;
        let query = '';

        let getInstitutions = db.queryAsync('SELECT * FROM institutions');
        let getUsers = db.queryAsync('SELECT * FROM users');
        let getMentors = db.queryAsync('SELECT * FROM mentors');
        let getCompetitors = db.queryAsync('SELECT * FROM competitors');
        let getRounds = db.queryAsync('SELECT * FROM rounds');

        switch (type) {
            case 'institutions':
                query = [getInstitutions, getUsers];
                break;
            case 'mentors':
                query = [getMentors];
                break;
            case 'competitors':
                query = [getCompetitors];
                break;
            case 'rounds':
                let roundId = req.query.id;
                let getRoundsMap = db.queryAsync('SELECT * FROM rounds_map WHERE round_id = ' + roundId);
                let getRound = db.queryAsync('SELECT * FROM rounds WHERE id = ' + roundId);
                query = [getRoundsMap, getRound, getInstitutions, getMentors, getCompetitors, getUsers];
                break;
        }

        return P.all(query)
            .then((response) => {
                return response;
            })
            .catch((err) => {
                logger.error(err);
                throw new exception(err);
            });
    }
}

module.exports = new ExportService();