const jwt = require('jsonwebtoken');
const settings = require('../../../settings');

module.exports = (req, res, next) => {
    let token;

    if (req.method === 'OPTIONS') {
        next();
    } else {
        token = req.get('Authorization');

        jwt.verifyAsync(token, settings.jwt.secretPassPhrase).then(() => {
            next();
        }).catch((err) => {
            res.status(401).json('Token invalid.');
        });
    }
};