const _ = require('lodash');

module.exports = (omit, middleware) => {
    return (req, res, next) => {
        let omitted = false;
        _.each(omit, (item) => {
            if (item.path === req.path && item.method === req.method) {
                omitted = true;
            }
        });

        return omitted ? next() : middleware(req, res, next);
    };
};