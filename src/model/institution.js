class Institution {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.address = data.address;
        this.postcode = data.postcode;
        this.oib = data.oib;
        this.city = data.city;
        this.country = data.country;
        this.director = data.director;
        this.directorEmail = data.director_email;
        this.directorPhone = data.director_phone;
        this.isInterestedInOrganisation = !!data.interested_in_organisation;
        this.isRegionalCenter = !!data.regional_center;
        this.regionalCenterName = data.regional_center_name;
    }
}

module.exports = Institution;