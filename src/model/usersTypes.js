const db = require('../db');

class UserType {
    constructor(data) {
        this.id = data.id;
        this.type = data.type;
    }
}

let usersTypes = {};
db.query('SELECT * FROM user_type', (err, rows) => {
    rows.forEach((row) => {
        usersTypes[row.id] = new UserType(row);
    });
});

module.exports = usersTypes;