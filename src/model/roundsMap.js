class RoundsMap {
    constructor(data) {
        this.roundId = data.round_id;
        this.institutionId = data.institution_id;

        this.mentors = [{
            id: data.mentor1_id,
            compeatingGroup: data.mentor1_compeating_group
        }, {
            id: data.mentor2_id,
            compeatingGroup: data.mentor2_compeating_group
        }];

        this.competitors = [{
            id: data.competitor1_id,
            compeatingGroup: data.competitor1_compeating_group
        }, {
            id: data.competitor2_id,
            compeatingGroup: data.competitor2_compeating_group
        }, {
            id: data.competitor3_id,
            compeatingGroup: data.competitor3_compeating_group
        },{
            id: data.competitor4_id,
            compeatingGroup: data.competitor4_compeating_group
        }];
    }
}

module.exports = RoundsMap;