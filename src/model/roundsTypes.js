const db = require('../db');

class RoundType {
    constructor(data) {
        this.id = data.id;
        this.type = data.type;
        this.displayName = data.display_name;
    }
}

let roundsTypes = {};
db.query('SELECT * FROM round_type', function (err, rows) {
    rows.forEach(function (row) {
        roundsTypes[row.id] = new RoundType(row);
    });
});

module.exports = roundsTypes;