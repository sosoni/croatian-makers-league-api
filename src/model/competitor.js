const competitorsAgeGroups = require('./competitorsAgeGroups');

class Competitor {
    constructor(data) {
        this.id = data.id;
        this.institutionId = data.institution_id;
        this.name = data.name;
        this.lastname = data.lastname;
        this.birthdate = data.birthdate;
        this.class = data.class;
        this.sex = data.sex;
        this.approvalDoc = data.approval_doc;
        this.approved = !!data.approved;
        this.ageGroup = competitorsAgeGroups[data.competitors_age_group_id];
    }
}

module.exports = Competitor;