const db = require('../db');

class CompetitorAgeGroup {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.displayName = data.display_name;
    }
}

let ageGroups = {};
db.query('SELECT * FROM competitors_age_group', function (err, rows) {
    rows.forEach(function (row) {
        ageGroups[row.id] = new CompetitorAgeGroup(row);
    });
});

module.exports = ageGroups;