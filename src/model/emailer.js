const nodemailer = require('nodemailer');
const P = require('bluebird');

const settings = require('../../settings');

const transporterOptions = {
    support: {
        service: settings.emailer[settings.server.env].support.service,
        auth: {
            user: settings.emailer[settings.server.env].support.user,
            pass: settings.emailer[settings.server.env].support.pass
        }
    }
};

class Emailer {
    constructor(type) {
        this.transporterOptions = transporterOptions[type];
    }

    send(data) {
        // Check if emailer is enabled
        if (!settings.emailer.enabled) return P.reject('Emailer disabled!');

        let message = {
            from: data.from,
            to: data.to,
            subject: data.subject,
            text: data.text
        };

        let transporter = this.getTransporter();
        return transporter.sendMail(message);
    }

    getTransporter() {
        return P.promisifyAll(nodemailer.createTransport(this.transporterOptions));
    }
}

module.exports = Emailer;
