const mentorsAgeGroups = require('./mentorsAgeGroups');

class Mentor {
    constructor(data) {
        this.id = data.id;
        this.institutionId = data.institution_id;
        this.name = data.name;
        this.email = data.email;
        this.phone = data.phone;
        this.ageGroup = mentorsAgeGroups[data.mentors_age_group_id];
    }
}

module.exports = Mentor;