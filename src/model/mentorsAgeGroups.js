const db = require('../db');

class MentorAgeGroup {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.displayName = data.display_name;
    }
}

let ageGroups = {};
db.query('SELECT * FROM mentors_age_group', function (err, rows) {
    rows.forEach(function (row) {
        ageGroups[row.id] = new MentorAgeGroup(row);
    });
});

module.exports = ageGroups;