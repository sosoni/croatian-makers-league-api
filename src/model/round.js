const roundsTypes = require('./roundsTypes');

class Round {
    constructor(data) {
        this.id = data.id;
        this.type = roundsTypes[data.round_type_id];
        this.name = data.name;
        this.created = data.created;
        this.opened = !!data.opened;
        this.googleSheets = data.google_sheets;
        this.googleSheets2 = data.google_sheets2;
    }
}

module.exports = Round;