const usersTypes = require('./usersTypes');

class User {
    constructor(data) {
        this.email = data.email;
        this.type = usersTypes[data.user_type_id].type;
        this.authorized = !!data.authorized;

        // Add `institutionId` property to basic user
        if (usersTypes[data.user_type_id].id === 2) {
            this.institutionId = data.user_institution_id;
        }
    }
}

module.exports = User;