const winston = require('winston');

// Create logger
let logger = new (winston.Logger)({
    transports: [
        new (winston.transports.File)({
            name: 'error-file',
            filename: cml.path + '/logs/error.log',
            level: 'error'
        })
    ]
});

// Export
module.exports = logger;