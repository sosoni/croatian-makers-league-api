const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');

const settings = require('../settings');
const hostname = settings.server[settings.server.env].hostname;
const port = settings.server[settings.server.env].port;

// Routes
const emailer = require('./common/emailer/emailer');
const jwtMiddleware = require('./common/middleware/jwt');
const omitMiddleware = require('./common/middleware/omit');

// Set app
const app = express();
app.disable('x-powered-by');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use((req, res, next) => {
    let origin = req.headers.origin;
    if (settings.allowedOrigins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Authorization, Content-Type');
    res.setHeader('Access-Control-Expose-Headers', 'Access-Token');
    res.setHeader('Access-Control-Allow-Credentials', true);
    return next();
});
app.use(omitMiddleware(settings.omitMiddlewareList, jwtMiddleware));

// Handle routes
app.post('/emailer-support', emailer.support);
app.use('/excel', require('./common/excel-export/router'));
app.use('/auth', require('./components/auth/router'));
app.use('/materials', require('./components/materials/router'));
app.use('/institutions', require('./components/institutions/router'));
app.use('/mentors', require('./components/mentors/router'));
app.use('/competitors', require('./components/competitors/router'));
app.use('/results', require('./components/results/router'));
app.use('/rounds', require('./components/rounds/router'));
app.use('/password', require('./components/password_reset/router'));

module.exports = {
    start() {
        // Run server
        http.createServer(app).listen(port, hostname);
        console.log(`Server running at http://${hostname}:${port}/`);
    }
};
