#Croatian Makers League API

NodeJS application.

Steps to run server part:
----------
###Server:
 * git clone
 * npm install
 * node index.js (or node_modules/nodemon/bin/nodemon index.js or using pm2)

Mysql connection setup
-----------
###Steps
 * add `db.json` to server api (edit with database connection info)
 * import `cml.sql` to database

 Example of `db.json`:
```json
{
    "host": "localhost",
    "user": "phpmyadmin_user",
    "password": "phpmyadmin_password",
    "database": "cml"
}
```

#Runing application on production
 * update `settings.js`
 * set server.env as `production`
 * set emailer.enabled as `true`