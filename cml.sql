-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 27, 2017 at 10:32 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cml`
--
CREATE DATABASE IF NOT EXISTS `cml` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cml`;

-- --------------------------------------------------------

--
-- Table structure for table `competitors`
--

CREATE TABLE `competitors` (
  `id` int(11) NOT NULL,
  `institution_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `class` int(11) NOT NULL,
  `sex` varchar(1) NOT NULL,
  `approval_doc` varchar(255) DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `competitors_age_group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `competitors`
--

INSERT INTO `competitors` (`id`, `institution_id`, `name`, `lastname`, `birthdate`, `class`, `sex`, `approval_doc`, `approved`, `competitors_age_group_id`) VALUES
(33, 15, 'Marko', 'Maric', '2017-09-01', 1, 'Ž', NULL, 1, 1),
(34, 15, 'Marija', 'Maric', '2017-09-01', 2, 'Ž', NULL, 1, 1),
(35, 15, 'Frano', 'Barić', '2017-09-05', 6, 'M', NULL, 1, 2),
(36, 15, 'Danijela', 'Ćurić', '2017-09-06', 8, 'Ž', NULL, 1, 2),
(37, 15, 'Martina', 'Martinčić', '2017-09-06', 2, 'Ž', NULL, 1, 1),
(38, 15, 'Franjo', 'Ramljak', '2017-09-07', 4, 'M', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `competitors_age_group`
--

CREATE TABLE `competitors_age_group` (
  `id` int(11) NOT NULL,
  `name` varchar(7) NOT NULL,
  `display_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `competitors_age_group`
--

INSERT INTO `competitors_age_group` (`id`, `name`, `display_name`) VALUES
(1, 'younger', 'Mlađi (1. - 5.)'),
(2, 'older', 'Stariji (6. - 9.)');

-- --------------------------------------------------------

--
-- Table structure for table `institutions`
--

CREATE TABLE `institutions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `postcode` varchar(30) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `oib` varchar(30) NOT NULL,
  `director` varchar(255) NOT NULL,
  `director_email` varchar(100) NOT NULL,
  `director_phone` varchar(30) NOT NULL,
  `interested_in_organisation` tinyint(1) DEFAULT NULL,
  `regional_center` tinyint(1) DEFAULT NULL,
  `regional_center_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `institutions`
--

INSERT INTO `institutions` (`id`, `name`, `address`, `postcode`, `city`, `country`, `oib`, `director`, `director_email`, `director_phone`, `interested_in_organisation`, `regional_center`, `regional_center_name`) VALUES
(14, 'Prva osnovna', 'adresa', '1234532', 'grad', 'BiH', '1234', 'ravnatelj', 'ravnatelj@gmail.com', '42309543', 1, 1, 'Prva osnovna (grad)'),
(15, 'OS Ivana Gundulica', 'Rudnik 123', '88000', 'Mostar', 'BiH', '423432543', 'Marko Perkovic', 'marko_perkovic@gmail.com', '43243242332', 1, 1, 'OS Ivana Gundulica (Mostar)'),
(16, 'OS Marka Marulica', 'Vukovarska 12', '88000', 'Mostar', 'BiH', '234423', 'Franjo Ramljak', 'franjo_@gmail.com', '43243223423', 1, 1, 'OS Marka Marulica (Mostar)'),
(17, 'OS Peta osnovna', 'Adresa', '432423', 'Mostar', 'BiH', '1254325', 'Mario Matic', 'gregr@gmail.com', '43253434534', 1, 0, NULL),
(18, 'Naziv23', 'adresa', '1234232', 'grad', 'BiH', '12', 'frwefger', 'ravnatelj234@gmail.com', '324235', 1, 0, 'Prva osnovna (grad)'),
(19, 'Testni naziv ustanove', 'testna adresa', '0', 'Mostar', 'BiH', '123324', 'testni ravnatelj', 'fgreige@gmail.com', 'fjprofgre', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE `materials` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `visibility` text NOT NULL,
  `extension` varchar(25) NOT NULL,
  `encoding` varchar(40) NOT NULL,
  `mime_type` varchar(25) NOT NULL,
  `size` int(12) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mentors`
--

CREATE TABLE `mentors` (
  `id` int(11) NOT NULL,
  `institution_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `mentors_age_group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mentors`
--

INSERT INTO `mentors` (`id`, `institution_id`, `name`, `email`, `phone`, `mentors_age_group_id`) VALUES
(24, 14, 'Ante Maric', 'ante_maric@gmail.com', '123342 2434', 1),
(25, 14, 'Marko Perkovic', 'marko_perkovic@gmail.com', '423 4324 1242', 2),
(26, 15, 'Mario Matic', 'mario_matic@gmail.com', '3124 14 423', 1),
(27, 15, 'Jure Markovic', 'markovic.jure@gmail.com', '5423 24235 523', 2),
(28, 15, 'Danijela Maric', 'maric.danijela@gmail.com', '063/122-233', 3),
(31, 15, 'Ivana Juric', 'juric.ivana@gmail.com', '063/125-126', 3);

-- --------------------------------------------------------

--
-- Table structure for table `mentors_age_group`
--

CREATE TABLE `mentors_age_group` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `display_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mentors_age_group`
--

INSERT INTO `mentors_age_group` (`id`, `name`, `display_name`) VALUES
(1, 'younger', 'Mlađi (1. - 5.)'),
(2, 'older', 'Stariji (6. - 9.)'),
(3, 'all', 'Svi');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE `password_reset` (
  `id` int(11) NOT NULL,
  `req_uuid` varchar(36) NOT NULL,
  `email` varchar(40) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`id`, `name`, `link`, `created`) VALUES
(2, '2016-2017', 'http://2016-2017.com', '2017-09-14 22:26:55'),
(3, '2017-2018', 'http://2017-2018.com', '2017-09-14 22:27:12');

-- --------------------------------------------------------

--
-- Table structure for table `rounds`
--

CREATE TABLE `rounds` (
  `id` int(11) NOT NULL,
  `round_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `opened` tinyint(1) NOT NULL DEFAULT '1',
  `google_sheets` varchar(255) DEFAULT NULL,
  `google_sheets2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rounds`
--

INSERT INTO `rounds` (`id`, `round_type_id`, `name`, `created`, `opened`, `google_sheets`, `google_sheets2`) VALUES
(32, 2, 'Fizicko kolo', '2017-08-31 21:44:38', 0, 'http://some.google.sheets', 'http://bodovanje.com'),
(33, 1, 'Online kolo', '2017-08-31 21:46:45', 0, 'http://some.google.sheets', NULL),
(34, 2, 'Fizicko kolo2', '2017-08-31 21:47:13', 0, '', NULL),
(35, 1, 'Novo online kolo', '2017-09-11 20:37:55', 1, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rounds_map`
--

CREATE TABLE `rounds_map` (
  `id` int(11) NOT NULL,
  `round_id` int(11) NOT NULL,
  `institution_id` int(11) NOT NULL,
  `mentor1_id` int(11) DEFAULT NULL,
  `mentor1_compeating_group` int(11) DEFAULT NULL,
  `mentor2_id` int(11) DEFAULT NULL,
  `mentor2_compeating_group` int(11) DEFAULT NULL,
  `competitor1_id` int(11) DEFAULT NULL,
  `competitor1_compeating_group` int(11) DEFAULT NULL,
  `competitor2_id` int(11) DEFAULT NULL,
  `competitor2_compeating_group` int(11) DEFAULT NULL,
  `competitor3_id` int(11) DEFAULT NULL,
  `competitor3_compeating_group` int(11) DEFAULT NULL,
  `competitor4_id` int(11) DEFAULT NULL,
  `competitor4_compeating_group` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rounds_map`
--

INSERT INTO `rounds_map` (`id`, `round_id`, `institution_id`, `mentor1_id`, `mentor1_compeating_group`, `mentor2_id`, `mentor2_compeating_group`, `competitor1_id`, `competitor1_compeating_group`, `competitor2_id`, `competitor2_compeating_group`, `competitor3_id`, `competitor3_compeating_group`, `competitor4_id`, `competitor4_compeating_group`) VALUES
(14, 32, 15, 31, 1, 27, 2, 34, 1, 38, 1, 33, 2, 35, 2),
(17, 35, 15, NULL, NULL, NULL, NULL, 34, 1, NULL, NULL, 38, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `round_type`
--

CREATE TABLE `round_type` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `display_name` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `round_type`
--

INSERT INTO `round_type` (`id`, `type`, `display_name`) VALUES
(1, 'online', 'Online kolo'),
(2, 'physical', 'Fizičko kolo');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL DEFAULT '2',
  `user_institution_id` int(11) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `authorized` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type_id`, `user_institution_id`, `email`, `password`, `authorized`) VALUES
(3, 1, NULL, 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 1),
(4, 2, 2, 'user@gmail.com', 'ee11cbb19052e40b07aac0ca060c23ee', 1),
(35, 2, 14, 'ravnatelj2@gmail.com', '202cb962ac59075b964b07152d234b70', 1),
(36, 2, 15, 'ravnatelj1@gmail.com', '202cb962ac59075b964b07152d234b70', 1),
(37, 2, 16, 'ravnatelj3@gmail.com', '202cb962ac59075b964b07152d234b70', 1),
(38, 2, 17, 'ravnatelj5@gmail.com', '202cb962ac59075b964b07152d234b70', 0),
(39, 1, NULL, 'kovacev.josip@gmail.com', '202cb962ac59075b964b07152d234b70', 1),
(40, 2, NULL, 'ravnatelj234@gmail.com', '202cb962ac59075b964b07152d234b70', 0),
(42, 2, NULL, 'ravnatelj23456@gmail.com', '7363a0d0604902af7b70b271a0b96480', 0),
(44, 2, 18, 'ravnatelj234566@gmail.com', '30cd2f99101cdd52cc5fda1e996ee137', 1),
(45, 2, NULL, 'r123@gmail.com', '202cb962ac59075b964b07152d234b70', 0),
(47, 2, NULL, 'r1234@gmail.com', '7363a0d0604902af7b70b271a0b96480', 0),
(48, 2, NULL, 'ravnatelj1235454@gmail.com', '202cb962ac59075b964b07152d234b70', 0),
(50, 2, NULL, 'ravnatelj12354541@gmail.com', '202cb962ac59075b964b07152d234b70', 0),
(51, 2, NULL, 'ravnatelj_43209403543@gmail.com', '202cb962ac59075b964b07152d234b70', 0),
(53, 2, 19, 'ravnatelj_432094203543@gmail.com', '202cb962ac59075b964b07152d234b70', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `type`) VALUES
(1, 'admin'),
(2, 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `competitors`
--
ALTER TABLE `competitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `competitors_age_group`
--
ALTER TABLE `competitors_age_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institutions`
--
ALTER TABLE `institutions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `mentors`
--
ALTER TABLE `mentors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mentors_age_group`
--
ALTER TABLE `mentors_age_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rounds`
--
ALTER TABLE `rounds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rounds_map`
--
ALTER TABLE `rounds_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `round_type`
--
ALTER TABLE `round_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `competitors`
--
ALTER TABLE `competitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `competitors_age_group`
--
ALTER TABLE `competitors_age_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `institutions`
--
ALTER TABLE `institutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `materials`
--
ALTER TABLE `materials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mentors`
--
ALTER TABLE `mentors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `mentors_age_group`
--
ALTER TABLE `mentors_age_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `password_reset`
--
ALTER TABLE `password_reset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `rounds`
--
ALTER TABLE `rounds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `rounds_map`
--
ALTER TABLE `rounds_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `round_type`
--
ALTER TABLE `round_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
