// Create global namespace
global.cml = {
    path: __dirname
};

// Define variables
const app = require('./src/app');
const promise = require('bluebird');
const jwt = require('jsonwebtoken');

//Promisify neccesarry libs
promise.promisifyAll(jwt);
promise.promisifyAll(require('mysql/lib/Connection').prototype);

// Start the app
app.start();