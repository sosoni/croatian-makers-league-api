module.exports = {
    server: {
        env: 'dev',
        dev: {
            hostname: '127.0.0.1',
            port: 8000
        },
        production: {
            hostname: 'www.sparkreators.com',
            port: 8000
        }
    },
    allowedOrigins: [
        'http://localhost:3000',
        'http://sparkreators.com',
        'http://www.sparkreators.com'
    ],
    // List of JWT ommited routes
    omitMiddlewareList: [
        { path: '/auth/login', method: 'POST' },
        { path: '/auth/logout', method: 'POST' },
        { path: '/institutions/create', method: 'POST' },
        { path: '/password/reset', method: 'POST' },
        { path: '/password/set_new', method: 'POST' },
        { path: '/password/reset_check', method: 'GET' }
    ],
    emailer: {
        enabled: false,
        dev: {
            support: {
                service: 'gmail',
                user: 'cml.supp@gmail.com',
                pass: '1234Abcd'
            }
        },
        production: {
            support: {
                service: 'gmail',
                user: 'sparkreators@gmail.com',
                pass: 'sparkreatorsmostar1'
            }
        },
        content: {
            registration: {
                subject: 'Registracija',
                text: 'Poštovani, \n\ruspješno ste se registrirali. \n\rAdministrator će odobriti pristup aplikaciji u roku od 24 sata.'
            },
            passReset: {
                subject: 'Password Reset',
                text: 'Da bi resetirali lozinku, molimo kliknite na sljedeći link: \n \r'
            },
            activation: {
                subject: 'Aktivacija',
                text: 'Poštovani, \n\rVaš račun je aktiviran.'
            },
            deactivation: {
                subject: 'Deaktivacija',
                text: 'Poštovani, \n\rVaš račun je deaktiviran.'
            }
        }
    },
    jwt: {
        secretPassPhrase: 'cmleague',
        tokenDuration: 28800//  8 hours in seconds
    },
    fileUpload: {
        maxFiles: 12,
        fileLimit: 10485760 // 10 MB per file
    },
    passwordReset: {
        expiryDuration: 3600 // 1 hour
    }
};